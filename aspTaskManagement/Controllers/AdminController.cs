﻿using aspTaskManagement.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace aspTaskManagement.Controllers
{   
    [Authorize(Roles ="SuperAdmin, Admin")]
    public class AdminController : Controller
    {

        ApplicationDbContext context = new ApplicationDbContext();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(FormCollection form)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (ModelState.IsValid)
            {
                string UserName = form["txtEmail"];
                string email = form["txtEmail"];
                string pwd = form["txtPassword"];

                //***********************************
                //Create Users Profile
                Profile profile = new Profile();
                profile.Username = UserName;
                context.Profiles.Add(profile);
                context.SaveChanges();
                //***********************************

                //create new  user
                var user = new ApplicationUser();
                user.UserName = UserName;
                user.Email = email;


                var newUser = userManager.Create(user, pwd); 
                return Redirect("/Home");
            }

            return View(form);
        }

        public ActionResult AssignRole()
        {

            ViewBag.Email = context.Users.Select(r => new SelectListItem { Value = r.Email, Text = r.UserName }).ToList();
            ViewBag.Roles = context.Roles.Select(r => new SelectListItem { Value = r.Name, Text = r.Name }).ToList();

            return View();
        }

        [HttpPost]
        public ActionResult AssignRole(FormCollection form)
        {
            string username = form["Email"];
            string rolname = form["RoleName"];
            ApplicationUser user = context.Users.Where(u=>u.UserName.Equals(username, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            userManager.AddToRole(user.Id, rolname);
            return View("Index");
        }

        [HttpPost]
        public ActionResult NewRole(FormCollection form)
        {
            string roleName = form["RoleName"];
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            if (!roleManager.RoleExists(roleName))
            {
                //create super admin
                var role = new IdentityRole(roleName);
                roleManager.Create(role);
            }

            return View("Index");
        }

        public ActionResult CreateRole()
        {
            return View();
        }

      
        public ActionResult EditProfile(RegisterViewModel model)
        {
            string id = User.Identity.GetUserId();

            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var getUser = userManager.FindById(id);
            if (getUser == null)
            {
                return HttpNotFound();
            }

           

            model.Email = getUser.Email;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult postProfile([Bind(Include ="Email")]  RegisterViewModel model)
        {
            string Email = model.Email;

            if (ModelState.IsValid)
            {


            }

            return View(model);
        }

        [HttpGet]
        public ActionResult UsersList()
        {

            var userList = context.Users.ToList();

            List<ApplicationUser> _userList = new List<ApplicationUser>();

            _userList.AddRange(userList);

            return View(_userList);
        }

    }
}