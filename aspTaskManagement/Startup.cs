﻿using aspTaskManagement.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using System.Collections.Generic;
using System.Linq;


[assembly: OwinStartupAttribute(typeof(aspTaskManagement.Startup))]
namespace aspTaskManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateUserAndRoles();
        }

        public void CreateUserAndRoles()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));



            var count = userManager.Users.Count();
            if (count == 0)
            {
                if (!roleManager.RoleExists("SuperAdmin"))
                {
                    //Create Super admin Role
                    var role = new IdentityRole("SuperAdmin");
                    roleManager.Create(role);

                    //create default user
                    var user = new ApplicationUser();
                    user.UserName = "admin@gmail.com";
                    user.Email = "admin@gmail.com";
                    string pwd = "Dominador123!";

                    //Create users profile
                    Profile profile = new Profile();
                    profile.Username = user.UserName;
                    context.Profiles.Add(profile);
                    context.SaveChanges();

                    var newuser = userManager.Create(user, pwd);
                    if (newuser.Succeeded)
                    {
                        userManager.AddToRole(user.Id, "SuperAdmin");
                        
                    }

                }
            }

        }
    }
}
